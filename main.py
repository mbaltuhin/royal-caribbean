import math
import os
from datetime import datetime, timedelta

import requests
import xlsxwriter


def write_file_to_excell(array):
    userhome = os.path.expanduser('~')
    now = datetime.now()
    path_to_file = userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(
        now.day) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-Royal Caribbean.xlsx'
    if not os.path.exists(userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(
            now.month) + '-' + str(now.day)):
        os.makedirs(
            userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(now.day))
    workbook = xlsxwriter.Workbook(path_to_file)

    worksheet = workbook.add_worksheet()
    worksheet.set_column("A:A", 15)
    worksheet.set_column("B:B", 25)
    worksheet.set_column("C:C", 10)
    worksheet.set_column("D:D", 25)
    worksheet.set_column("E:E", 20)
    worksheet.set_column("F:F", 30)
    worksheet.set_column("G:G", 20)
    worksheet.set_column("H:H", 50)
    worksheet.set_column("I:I", 20)
    worksheet.set_column("J:J", 20)
    worksheet.set_column("K:K", 20)
    worksheet.set_column("L:L", 20)
    worksheet.set_column("M:M", 25)
    worksheet.set_column("N:N", 20)
    worksheet.set_column("O:O", 20)
    worksheet.set_column("P:P", 21)
    worksheet.write('A1', 'DestinationCode')
    worksheet.write('B1', 'DestinationName')
    worksheet.write('C1', 'VesselID')
    worksheet.write('D1', 'VesselName')
    worksheet.write('E1', 'CruiseID')
    worksheet.write('F1', 'CruiseLineName')
    worksheet.write('G1', 'ItineraryID')
    worksheet.write('H1', 'BrochureName')
    worksheet.write('I1', 'NumberOfNights')
    worksheet.write('J1', 'SailDate')
    worksheet.write('K1', 'ReturnDate')
    worksheet.write('L1', 'InteriorBucketPrice')
    worksheet.write('M1', 'OceanViewBucketPrice')
    worksheet.write('N1', 'BalconyBucketPrice')
    worksheet.write('O1', 'SuiteBucketPrice')
    worksheet.write('P1', 'PortList')
    col = 0
    row = 1
    for result in array:
        for item in result:
            if col == {11, 12, 13, 14}:
                try:
                    worksheet.write_number(row, col, item)
                except ValueError:
                    worksheet.write_string(row, col, str(item))
            elif col == {9, 10}:
                worksheet.write_datetime(row, col, item)
            else:
                worksheet.write_string(row, col, str(item))
            col += 1
        row += 1
        col = 0
    workbook.close()


headers = {
    "referer": "ttps://www.royalcaribbean.com/cruises/?country=USA",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36",
    "x-requested-with": "XMLHttpRequest",
}
response = requests.get(
    "https://www.royalcaribbean.com/ajax/cruises/service/lookup?initialLoad=false&loadLinks=true&market=usa&country=USA&language=en&country=USA&currentPage=1",
    headers=headers).json()
total_results = response["listResultsModule"]["totalResults"]
total_pages = math.ceil(total_results/10)
print("Total Results", total_results)
current_page = 0
results = []
parsed_results = []
while current_page < total_pages:
    print("Working with page", current_page+1, "from", total_pages)
    response = requests.get(
        "https://www.royalcaribbean.com/ajax/cruises/service/lookup?initialLoad=false&loadLinks=true&market=usa&country=USA&language=en&country=USA&currentPage="+str(current_page),
        headers=headers).json()
    current_page_results = response["listResultsModule"]["resultData"]["pageResults"]
    for result in current_page_results:
        results.append(result)
    current_page += 1
for result in results:
    destination_code = result["destinationCode"]
    title = result["displayName"]
    duration = result["sailingNights"]
    ship_name = result["ship"]["name"]
    itinerary_id = result["id"]
    for sailing in result["sailings"]:
        ports = []
        for port in sailing["cruisePorts"]:
            ports.append(port["name"])
        cruise_id = sailing["cabinLaf"]["packageId"]
        interior = "N/A"
        outside = "N/A"
        balcony = "N/A"
        suite = "N/A"
        for room in sailing["priceRanges"]:
            if room["category"] in "INTERIOR":
                if room["amount"] != 0:
                    interior = int(room["amount"])
            elif room["category"] in "OUTSIDE":
                if room["amount"] != 0:
                    outside = int(room["amount"])
            elif room["category"] in "BALCONY":
                if room["amount"] != 0:
                    balcony = int(room["amount"])
            elif room["category"] in "DELUXE":
                if room["amount"] != 0:
                    suite = int(room["amount"])

        sail_date = datetime.strptime(sailing["startDate"], '%Y-%m-%d')
        arrive_date = sail_date + timedelta(days=duration)
        parsed_results.append(
            [destination_code, destination_code, "", ship_name, cruise_id, "Royal Caribbean", itinerary_id, title,
             duration, sail_date.date(), arrive_date.date(), interior, outside, balcony,
             suite, ports])
write_file_to_excell(parsed_results)
input("Press ENTER key twice to continue...")